package app

import (
	"eurest-mysql-test/config"
	"eurest-mysql-test/database"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/TwiN/go-color"
	"github.com/urfave/cli"
)

func Generate() *cli.App {
	app := cli.NewApp()
	app.Name = "MySQL Connection Tester"
	app.Usage = "Tests connections to a MySQL server."

	flags := []cli.Flag{
		cli.IntFlag{
			Name:  "procs",
			Value: 1,
		},
		cli.StringFlag{
			Name:  "host",
			Value: "zscloud6.zonesoft.org",
		},
		cli.StringFlag{
			Name:  "port",
			Value: "3306",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:   "run",
			Usage:  "Number of processes to execute.",
			Flags:  flags,
			Action: RunTest,
		},
	}
	return app
}

func RunTest(c *cli.Context) {

	host := c.String("host")
	port := c.String("port")
	processes := c.Int("procs")

	config.Load(port, host)

	file, err := os.OpenFile("log.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(file)

	fmt.Println(color.Colorize(color.Yellow, fmt.Sprintf("A ligar a %s:%s | Nº Processos: %d", config.Host, config.Port, processes)))

	for i := 1; i <= processes; i++ {
		db, err := database.Connect()
		if err != nil {
			log.Println(err)
			continue
		}
		activeUnits, err := database.CheckActiveUnits(db)
		if err != nil {
			log.Println(err)
			return
		}
		fmt.Println(color.Colorize(color.Cyan, strings.Repeat("-", 70)))
		fmt.Println(color.Colorize(color.Yellow, fmt.Sprintf("Processo nº %d", i)))
		for _, unit := range activeUnits {
			loja := unit.Loja
			unidade := unit.Unidade
			descricao := unit.Descricao

			fmt.Println(color.Colorize(color.Cyan, strings.Repeat("-", 70)))
			fmt.Println(color.Colorize(color.Red, fmt.Sprintf("Loja : %d", loja)))
			fmt.Println(color.Colorize(color.Red, fmt.Sprintf("Unidade: %s", unidade)))
			fmt.Println(color.Colorize(color.Red, fmt.Sprintf("Descrição: %s", descricao)))
		}
		fmt.Println(color.Colorize(color.Yellow, fmt.Sprintf("Processo nº %d terminado", i)))

		defer db.Close()
		time.Sleep(time.Second)
	}

	fmt.Println(color.Colorize(color.Cyan, strings.Repeat("-", 70)))
	fmt.Println("Prima ENTER para terminar...")
	fmt.Scanln()
	os.Exit(1)
}
