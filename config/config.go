package config

import (
	"fmt"
)

var (
	ConnectionString = ""
	User             = "zaapi"
	Password         = "507ZaapI507"
	Host             = "zscloud6.zonesoft.org"
	Port             = "3306"
	Database         = "zsrest_FUT7I5S8W2"
)

func Load(port, host string) {
	if port != "" {
		Port = port
	}

	if host != "" {
		Host = host
	}

	ConnectionString = fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		User,
		Password,
		Host,
		Port,
		Database,
	)
}
