package database

import (
	"database/sql"
	"eurest-mysql-test/config"
	"eurest-mysql-test/models"

	_ "github.com/go-sql-driver/mysql"
)

func Connect() (*sql.DB, error) {
	db, err := sql.Open("mysql", config.ConnectionString)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		db.Close()
		return nil, err
	}

	db.SetMaxOpenConns(4)
	db.SetMaxIdleConns(30)
	db.SetConnMaxIdleTime(30)
	db.SetConnMaxLifetime(300)

	return db, nil
}

func CheckActiveUnits(db *sql.DB) ([]models.Unit, error) {
	results, err := db.Query(
		`SELECT loja, unidade, descricao FROM eurest_unidades
		WHERE activa = 1
		AND unidade != 0`,
	)
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var units []models.Unit
	for results.Next() {
		var unit models.Unit
		if err := results.Scan(
			&unit.Loja,
			&unit.Unidade,
			&unit.Descricao,
		); err != nil {
			return nil, err
		}
		units = append(units, unit)
	}
	return units, nil
}
