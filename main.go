package main

import (
	"eurest-mysql-test/app"
	"log"
	"os"

	"github.com/TwiN/go-color"
)

func main() {

	app := app.Generate()
	if err := app.Run(os.Args); err != nil {
		log.Fatal(color.Colorize(color.Red, err.Error()))
	}
}
