package models

type Unit struct {
	Loja      int64
	Unidade   string
	Descricao string
	Activa    int8
}
